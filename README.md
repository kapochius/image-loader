## Image Loader
---

ImageLoader is React app that can load recent images using Flickr API. 

## Installation
---


```
npm install
```


## Usage
---
 
 
```
npm start
```


## Technologies
---
Project created using:

* react 16.10.2
* react-dom 16.10.2
* webpack 4.41.1
* babel 7.6.4
* eslint 6.1.0
