import React from 'react';
import FavoriteOverlay from '../src/components/FavoriteOverlay/index';
import renderer from 'react-test-renderer';

describe('FavoriteOverlay tests', () => {
  it('renders successfully & renders an Unfavorite button when item is favorited', () => {
    const component = renderer.create(
      <FavoriteOverlay isFavorite={true} />
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    let testInstance = component.root;
    expect(testInstance.find(node => node.type === 'button').props.children).toEqual('Unfavorite')
  });

  it ('renders a Favorite button when item is not favorited', () => {
    const component = renderer.create(
      <FavoriteOverlay isFavorite={false} />
    );

    let testInstance = component.root;
    expect(testInstance.find(node => node.type === 'button').props.children).toEqual('Favorite')
  });

  it ('renders "-" mark when no owner name is passed to props', () => {
    const component = renderer.create(
      <FavoriteOverlay isFavorite={true} />
    );

    let testInstance = component.root;
    expect(testInstance.findAllByType('p')[1].children[0]).toEqual('-');
  });

  it ('renders owner name when name is passed to props', () => {
    const props = {
      owner: 'Stanley'
    }

    const component = renderer.create(
      <FavoriteOverlay isFavorite={false} {...props}  />
    );

    let testInstance = component.root;
    expect(testInstance.findAllByType('p')[1].children[0]).toEqual(props.owner);
  });

  it ('renders "Untitled" when image has no title', () => {
    const component = renderer.create(
      <FavoriteOverlay isFavorite={false}  />
    );

    let testInstance = component.root;
    expect(testInstance.findAllByType('p')[0].children[0]).toEqual('Untitled');
  });

  it ('renders image title when it is passed through props', () => {
    const props = {
      title: 'TestTitle1'
    }

    const component = renderer.create(
      <FavoriteOverlay isFavorite={false} {...props} />
    );

    let testInstance = component.root;
    expect(testInstance.findAllByType('p')[0].children[0]).toEqual(props.title);
  });
});
