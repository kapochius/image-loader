import React from 'react';
import PhotoItemRender from '../src/components/PhotoItem/photoItemRender';
import renderer from 'react-test-renderer';
import Spinner from '../src/lib/placeholders/spinner';

describe('PhotoItemRender tests', () => {
  it('always renders spinner', () => {
    const component = renderer.create(
     <PhotoItemRender data={ [] }
                      fetchMoreCallback={() => { }}
                      handleItemVisibilityCallback={() => {}}
                      />
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    let testInstance = component.root;
    expect(testInstance.findByType(Spinner)).not.toBe(null);
  });
});
