import React from 'react';
import Gallery from '../src/components/Gallery/index';
import ImageLoader from '../src/api/ImageLoader';
import ImageVisibilityHandler from '../src/lib/imageVisibilityHandler';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

jest.mock('../src/lib/imageVisibilityHandler', () => () => ({

}));

jest.mock('../src/api/imageLoader', () => () => ({
  loadFromMainSource: () => { return new Promise(); },
  loadInitial:  () => { return new Promise(); },
}));

describe('Gallery tests', () => {
  it('should render correctly with no props', () => {
    const component = renderer.create(
      <Gallery/>
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
