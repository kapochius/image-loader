import React from 'react';
import PhotoItem from '../src/components/PhotoItem/photoItem';
import renderer from 'react-test-renderer';

describe('PhotoItem tests', () => {
  it('renders favorite mark if item is favorited', () => {
    const component = renderer.create(
      <PhotoItem key={1}
                 isFavorite={true} />
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    let testInstance = component.root;
    expect(testInstance.findByProps({ className: 'favorite-mark' })).not.toBe(null);
  });
});
