import React from 'react';
import LoadingCard from './LoadingCard';
import FavoriteStorage from '../storage/FavoriteStorage';

class LoadingCards extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      defaultCardCount: 6,
      initialLoadDone: this.props.initialLoadDone,
      favoritesCount: FavoriteStorage.favoriteCount
    }
  }

  getCardsCount = () => {
    var existingItems = this.props.initialLoadDone ? this.props.itemCount : this.state.favoritesCount;

    return existingItems % 3 === 0 ? 6 : existingItems % 3 === 1 ? 5 : existingItems % 3 === 2 ? 4 : this.state.defaultCardCount;
  }

  render(){
    const cardsCount = this.getCardsCount();

    return (
      <>
        {
          [...Array(cardsCount)].map((e, i) => <LoadingCard key={i} />)
        }
      </>
    );
  }
}

export default LoadingCards;
