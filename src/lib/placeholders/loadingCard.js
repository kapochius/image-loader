import React from 'react';

class LoadingCard extends React.Component {
  render(){
    return (
      <div className='col-sm-4 col-md-4 col-lg-4 d-flex align-items-stretch'>
        <span className="sr-only">Loading...</span>
        <div className='card'>
          <div className='card-body card-pulsating'>
            <img/>
            <div className="gradient placeholder-element"/>
          </div>
        </div>
      </div>
    );
  }
}

export default LoadingCard;
