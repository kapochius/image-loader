import React from 'react';

function Spinner() {
   return (
    <div className="bottom-spinner">
      <div className="bounce1"></div>
      <div className="bounce2"></div>
      <div className="bounce3"></div>
    </div>
  )
}

export default Spinner;
