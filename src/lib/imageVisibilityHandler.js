class ImageVisibilityHandler {
  visibilityOptions = {
    root: null,
    rootMargin: '0% 0% 20% 0%',
    treshold: 0.1
  }

  observer = null;

  constructor() {
    this.observer = new IntersectionObserver(this.onIntersection, this.visibilityOptions);
  }

  handleImgVisibility = () => {
    let images = [...document.querySelectorAll('.lazy-image')];

    images.forEach(image => this.observer.observe(image))
  }

  onIntersection = (imageEntities) => {
    imageEntities.forEach(lowQualityImage => {
      if (lowQualityImage.isIntersecting) {
        this.observer.unobserve(lowQualityImage.target)

        // Once LQ image is loaded, load HQ.
        lowQualityImage.target.onload = () => {
          lowQualityImage.target.classList.remove('lazy-image')
          lowQualityImage.target.parentNode.classList.remove('card-pulsating')

          let highQualityImage = lowQualityImage.target.nextElementSibling;
          highQualityImage.src = highQualityImage.dataset.src;

          highQualityImage.onload = () => {
            highQualityImage.classList.add('visible-image')
            lowQualityImage.target.classList.add('invisible-image')
          }
        }

        // If load fails, inform the user
        lowQualityImage.target.onerror = () => {
          let highQualityImage = lowQualityImage.target.nextElementSibling;
          highQualityImage.parentNode.classList.add('image-not-loaded');
          highQualityImage.classList.add('image-not-loaded')
        }

        lowQualityImage.target.src = lowQualityImage.target.dataset.src;
      }
    })
  }
}

export default ImageVisibilityHandler;
