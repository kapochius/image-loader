class FavoriteStorage {
  items = [];
  storageKey = "favorites";

  constructor() {
    if (!FavoriteStorage.instance) {
      var items = localStorage.getItem(this.storageKey);
      this.items = items ? JSON.parse(items) : [];

      FavoriteStorage.instance = this;
    }

    return FavoriteStorage.instance;
  }

  get anyFavorites() {
    return this.items && this.items.length > 0;
  }

  get favoriteCount() {
    return this.items && this.items.length;
  }

  get ids() {
    return this.items && this.items.map(x => x.id);
  }

  toggleFavorites(item) {
    if (!this.itemExists(item.id)) {
      this.add(item);
    } else {
      this.remove(item);
    }
  }

  itemExists(id) {
    if (!id) {
      throw new Error('Specify item id!');
    }

    return this.items.some(x => x.id === id);
  }

  add(item) {
    if (!item) {
      throw new Error(`Can't add empty item!`);
    }

    if (!item.id) {
      throw new Error('Item must have id!');
    }

    this.items = this.items.concat(item);
    localStorage.setItem(this.storageKey, JSON.stringify(this.items));
  }

  remove(item) {
    if (!item) {
      throw new Error(`Can't remove empty item!`);
    }

    this.items = this.items.filter((x) => {
      return x.id !== item.id
    });

    localStorage.setItem(this.storageKey, JSON.stringify(this.items));
  }
}

// Export as singleton
const instance = new FavoriteStorage();
export default instance;
