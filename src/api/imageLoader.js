import ApiClient from './ApiClient';
import ApiRequestParams from './ApiRequestParams';

class ImageLoader {
  _apiClient = new ApiClient();
  _requestParams = new ApiRequestParams();
  flickrByTagUrl = 'https://www.flickr.com/services/rest/?method=flickr.photos.search';
  apiKey = '290d88ef43d87832cad1a7ebc639e973';

  get initialUrl() {
    return `${this.flickrByTagUrl}
      &page=${this._requestParams.page}
      &per_page=${this._requestParams.initialPageSize}
      &tags=${this._requestParams.tags}
      &api_key=${this.apiKey}
      &extras=owner_name
      &format=json
      &nojsoncallback=1`.replace(/\s/g, '');
  }

  get byTagUrl() {
    return `${this.flickrByTagUrl}
      &page=${this._requestParams.page}
      &per_page=${this._requestParams.adjustedPageSize}
      &tags=${this._requestParams.tags}
      &api_key=${this.apiKey}
      &extras=owner_name
      &format=json
      &nojsoncallback=1`.replace(/\s/g, '');
  }

  async loadFromMainSource() {
    return this._apiClient.get(this.byTagUrl)
      .then(this.handleResponse);
  }

  async loadInitial() {
    return this._apiClient.get(this.initialUrl)
      .then(this.handleResponse);
  }

  setFavoritesCount(favoritesCount) {
    this._requestParams.setFavoritesCount(favoritesCount);
  }

  setTotalImagesDisplayed(imageCount){
    this._requestParams.setTotalImagesDisplayed(imageCount);
  }

  handleResponse = (data) => {
    return this.handleResponseData(data);
  }

  handleResponseData = (data) => {
    if (data.stat === 'ok') {
      this._requestParams.setPage();
      return Promise.resolve(data.photos.photo);
    }

    if (data.stat === 'fail') {
      return Promise.reject(data);
    }
  }
}

export default ImageLoader;
