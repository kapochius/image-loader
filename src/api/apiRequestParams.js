// eslint-disable-line no-unused-vars

class PagingParameters {
  favoritesCount = 0;
  page = 1;

  get tags() {
    return ['mountains', 'sea', 'italy', 'forest'].toString();
  }

  // Divisible by 3
  get pageSize() {
    if (window.innerHeight < 500) {
      return 12;
    } else if (window.innerHeight >= 500 && window.innerHeight < 2000) {
      return 18;
    } else if (window.innerHeight >= 2000 && window.innerHeight < 4000) {
      return 24;
    } else if (window.innerHeight >= 4000) {
      return 55;
    }

    return 18;
  }

  // If some items are filtered/discarded, we need to adjust for that
  get adjustedPageSize() {
    let defaultPageSize = this.pageSize;

    if (this.totalImagesCount % 3 === 0){
      return defaultPageSize;
    } else if (this.totalImagesCount % 3 === 1){
      return defaultPageSize + 2;
    } else if (this.totalImagesCount % 3 === 2){
      return defaultPageSize + 1;
    }

    return defaultPageSize;
  }

  get initialPageSize() {
    return this.favoritesCount % 3 === 0 ? this.pageSize : this.favoritesCount % 3 === 1 ? this.pageSize - 1 : this.favoritesCount % 3 === 2 ? this.pageSize - 2 : this.pageSize;
  }

  setFavoritesCount(favoritesCount) {
    this.favoritesCount = favoritesCount;
  }

  setTotalImagesDisplayed(imagesCount){
    this.totalImagesCount = imagesCount;
  }

  setPage(page) {
    this.page++;

    return this.page;
  }
}

export default PagingParameters;
