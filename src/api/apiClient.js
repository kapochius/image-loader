class ApiClient {
  get(url) {
    this.validateURL(url);

    return fetch(url)
      .then(this.handleResponseError)
      .then(res => res.text())
      .then((res) => {
        var result = JSON.parse(res);

        return result;
      })
      .catch((error) => {
        throw error;
      });
  }

  handleResponseError = (response) => {
    if (!response.ok || response.status < 200 || response.status > 300) {
      throw new Error(`Request failed. status: ${response.status}, status text: ${response.statusText} `);
    }

    // Everything is ok, return response to the pipeline
    return response;
  }

  validateURL(url) {
    if (!url) {
      throw new Error('URL not set!');
    }
  }
}

export default ApiClient;
