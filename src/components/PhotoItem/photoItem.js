import React from 'react';
import FavoriteOverlay from '../FavoriteOverlay';

class PhotoItem extends React.Component {
  constructor(props){
    super(props);
    this.urlPrefix = `https://farm${this.props.farm}.staticflickr.com/${this.props.server}/${this.props.id}_${this.props.secret}`;

    this.state = {
      url: `${this.urlPrefix}.jpg`,
      urlLowQuality: `${this.urlPrefix}_s.jpg`,
      title: this.props.itemName,
      isFavorite: this.props.isFavorite
    }
  }

  favoriteToggleCallback = (item) => {
    this.setState({ isFavorite: item.isFavorite})
    this.render();
  }

  render (){
    return (
      <div className='col-sm-4 col-md-4 col-lg-4 d-flex align-items-stretch'>
        <div className='card' >
          <div className='card-body card-pulsating'>
            <img data-src={this.state.urlLowQuality} className='img-fluid img-thumbnail image-inside-box lazy-image' />
            <img data-src={this.state.url} className='img-fluid img-thumbnail image-inside-box invisible-image' />
            <div className="gradient placeholder-element" />
            <div className="image-not-loaded-msg">
              <p>Error loading an image</p>
              <p>Check your internet connection</p>
            </div>
          </div>

          {
            this.state.isFavorite ?
              <span className="favorite-mark"/>
              :
              null
          }

          <div className='overlay'/>
          <FavoriteOverlay {...this.props} favoriteToggleCallback={this.favoriteToggleCallback}/>
        </div>
      </div>
    );
  }
}

export default PhotoItem;
