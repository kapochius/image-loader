import React from 'react'
import PhotoItem from './PhotoItem';
import InfiniteScroll from '../InfiniteScroll/infiniteScroll';
import Spinner from '../../lib/placeholders/spinner';

const PhotoItemRender = ({data, fetchMoreCallback, handleItemVisibilityCallback}) => {
  let i = 0;
  let isFetching = false;

  function isFetchingCallback(value){
    isFetching = value;
  }

  InfiniteScroll({fetchMoreCallback, handleItemVisibilityCallback, isFetchingCallback });

  return (
    <>
    <div className='row'>
      { data.map((data) => {
          return <PhotoItem key={ i++ }
                            id={data.id}
                            title={ data.title }
                            farm={data.farm}
                            server={data.server}
                            secret={data.secret}
                            isFavorite={data.isFavorite}
                            owner={data.ownername}
                            />
        })
      }
    </div>

    <div className='row'>
      <Spinner />
    </div>

    </>
  );
}

export default PhotoItemRender;
