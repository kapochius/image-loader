import React from 'react';
import FavoriteStorage from '../../lib/storage/FavoriteStorage';
import FavoriteButton from './FavoriteButton';

class FavoriteOverlay extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isFavorite: props.isFavorite
    };
  }

  toggleFavorites = () => {
    this.setState({ isFavorite: !this.state.isFavorite }, () => {
      const favoriteItem = Object.assign({}, this.props);

      favoriteItem.isFavorite = this.state.isFavorite;
      FavoriteStorage.toggleFavorites(favoriteItem);

      this.props.favoriteToggleCallback(favoriteItem);
    });
  }

  render(){
    return (
      <div className='favorite-overlay'>
        <h4 className='image-title-header'>
          <p className='image-title wrapped-text '>
            {this.props.title || 'Untitled'}
          </p>
        </h4>

        <hr/>

        <h4>
          <p className='image-owner-title wrapped-text'>
            {this.props.owner || '-'}
          </p>
        </h4>

        <FavoriteButton
          isFavorite={this.state.isFavorite}
          toggleFavorites={this.toggleFavorites}
        />
      </div>
    );
  }
}

export default FavoriteOverlay;
