import React from 'react';

class FavoriteButton extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <button type='button'
              className={'btn  btn-medium-large ' + (this.props.isFavorite ? 'btn-light' : 'btn-outline-light')}
              onClick={this.props.toggleFavorites}>

              {this.props.isFavorite ? 'Unfavorite' : 'Favorite'}
      </button>
    );
  }
}

export default FavoriteButton;
