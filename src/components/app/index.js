import React from 'react';
import './index.scss';
import Gallery from '../Gallery';

function App() {
  return (
      <Gallery />
  );
}

export default App;
