import { useState, useEffect } from 'react';
import debounce from '../../lib/helpers/functions/debounce';

const InfiniteScroll = ({fetchMoreCallback, handleItemVisibilityCallback, isFetchingCallback}) => {
  const [isFetching, setIsFetching] = useState(false);

  useEffect(() => {
    window.addEventListener('scroll', debounce(handleScrollAndResize, 150));
    window.addEventListener('resize', debounce(handleScrollAndResize, 150));
    return () => {
      window.removeEventListener('scroll', debounce(handleScrollAndResize, 150));
      window.removeEventListener('resize', debounce(handleScrollAndResize, 150));
    };
  }, []);

  useEffect(() => {
    if (!isFetching) return;
    fetchMoreListItems();
  }, [isFetching]);

  function fetchMoreListItems(){
      return fetchMoreCallback().then(() => {
        setIsFetching(false);
        isFetchingCallback(false);
      });
  }

  function handleScrollAndResize() {
    handleItemFetch();
    handleItemVisibilityCallback();
  }

  function handleItemFetch() {
    const visiblePagePercentage = getVisibilityCoefficient();
    var visibleScreen = window.innerHeight + window.pageYOffset
    var totalScreen = document.body.offsetHeight;

    if ((visibleScreen / totalScreen) >= visiblePagePercentage) {
        setIsFetching(true);
        isFetchingCallback(true);
    }
  }

  // Depends on the visible screen height.
  // The more height we have, the higher the coefficient should be to do less calls
  function getVisibilityCoefficient(){
    if (window.innerHeight < 500) {
      return 0.85;
    } else if (window.innerHeight >= 500 && window.innerHeight < 2000) {
      return 0.75;
    } else if (window.innerHeight >= 2000 && window.innerHeight < 4000) {
      return 0.65;
    } else if (window.innerHeight >= 4000) {
      return 0.90;
    }

    return 0.75;
  }

  return [isFetching, handleItemFetch, handleItemVisibilityCallback];
}

export default InfiniteScroll;
