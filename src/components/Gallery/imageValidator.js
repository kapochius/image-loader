import uniq from '../../lib/helpers/functions/uniq';

class ImageValidator {
  getUniqueFilteredArray(currentImages, newImages){
    const mergedImages = currentImages.concat(newImages);
    const uniqueArray = uniq(mergedImages, 'id');

    var filtered = this.filterImages(uniqueArray);

    return filtered;
  }

  filterImages(images){
    return images.filter(x => x.farm !== '0' && x.server !== '0');
  }
}

export default ImageValidator;
