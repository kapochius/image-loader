import React from 'react';
import ImageLoader from '../../api/ImageLoader';
import ImageValidator from './imageValidator';
import FavoriteStorage from '../../lib/storage/FavoriteStorage';
import ImageVisibilityHandler from '../../lib/imageVisibilityHandler';
import PhotoItemRender from '../photoItem/photoItemRender';

class Gallery extends React.Component {
  imageLoader = new ImageLoader();
  imageVisiblityHandler = new ImageVisibilityHandler();
  imageValidator = new ImageValidator();

  constructor(props){
    super(props);

    this.state = {
      imagesFromFlickr: [],
      favorites: [],
      favoritesCount: 0
    }
  }

  async componentDidMount(){
    this.removeRootSpinner();
    return this.handleLoad();
  }

  handleLoad(){
    if (FavoriteStorage.anyFavorites){
      this.setImages(FavoriteStorage.items);

      this.setState({
        favoritesCount: this.state.imagesFromFlickr.concat(FavoriteStorage.items).length
      }, () => {
        return this.loadInitial().then(() => {
            this.handleImgVisibility()
        });
      });

      return Promise.resolve();
    } else {
      return this.loadFromMainSource().then(() => {
          this.handleImgVisibility()
      });
    }
  }

  loadInitial(){
    this.imageLoader.setFavoritesCount(this.state.favoritesCount);

    return this.imageLoader.loadInitial().then((data) => {
      this.setImages(data);
      this.render();
    });
  }

  loadFromMainSource(){
    return this.imageLoader.loadFromMainSource()
      .then((data) => {
          this.setImages(data);
          this.render();

          return Promise.resolve(data);
      })
  }

  setImages(data){
    const images = this.imageValidator.getUniqueFilteredArray(this.state.imagesFromFlickr, data);
    this.imageLoader.setTotalImagesDisplayed(images.length);

    this.setState({
      imagesFromFlickr: images
    }, () => { this.handleImgVisibility() });
  }

  fetchMoreCallback = () => {
    return this.loadFromMainSource();
  }

  handleImgVisibility = () => {
    this.imageVisiblityHandler.handleImgVisibility();
  }

  removeRootSpinner(){
    const el = document.querySelector('#root-loading-spinner');
    el.remove();
  }

  render () {
    return (
      <div className='container'>
        <PhotoItemRender data={ this.state.imagesFromFlickr }
                         fetchMoreCallback={this.fetchMoreCallback}
                         handleItemVisibilityCallback={this.handleImgVisibility}
        />
      </div>
    );
  }
}

export default Gallery;
